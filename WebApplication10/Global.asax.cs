﻿using Repository.Infrastructure;
using Shared.Entities.DbEntities.ErrorLoging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace WebApplication10
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        void Application_Error(object sender, EventArgs e)
        {
            var ex = Context.Error;
   
            var s = new StackTrace(ex);
            var thisasm = Assembly.GetExecutingAssembly();


            var error = new ApplicationError
            {
                IssueDate = DateTime.Now,
                LoggedInUser = "Sree",
                Message = ex.Message,
                StackTrace = s.ToString(),
                MethodNameWithParamaters = ex.InnerException != null ? ex.InnerException.TargetSite.ToString() : "",
                Heading = ex.ToString(),
                InnerException = ex.InnerException != null ? ex.InnerException.ToString() : ex.ToString(),
            };

            using (var context = new DataContext())
            {
                var user = new  Team
                {
                    Name = "Blah",
                    Role =  Role.Test
                };


                context. Team.Add(user);
                context.ApplicationErrors.Add(error);

                context.CompleteOperation();
            }

                Server.ClearError();

            Response.Clear();
            //Response.Redirect("~/Error/CurrentError?id=" + error.Id);
        }
    }
}
