﻿namespace  Shared.Entities.DbEntities.Base
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}