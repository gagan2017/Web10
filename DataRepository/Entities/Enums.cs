﻿namespace  Shared.Entities.DbEntities.ErrorLoging
{
    public enum Priority
    {
        Low,
        Medium,
        High,
        Critical,
        Blocker
    }
    public enum BugState
    {
        Open,
        OnProgress,
        Fixed,
        Tested,
        Closed,
        Reopened,
        Duplicate,
        Invalid,
        KnownLimitation
    }

    public enum  Role
    {
        Dev,
        Test,
        Client

    }
}
