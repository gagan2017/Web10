﻿using  Shared.Entities.DbEntities.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace  Shared.Entities.DbEntities.ErrorLoging
{
    [Table("Error_CrashError")]
    public class ApplicationError : Entity
    {
        public virtual string Message { get; set; }
        public virtual string StackTrace { get; set; }
        public virtual DateTime IssueDate { get; set; }
        public virtual string LoggedInUser { get; set; }
        public virtual string MethodNameWithParamaters { get; set; }
        public virtual string Heading { get; set; }
        public virtual string InnerException { get; set; }
        
    }
}
 
