﻿using System.ComponentModel.DataAnnotations;

namespace  Shared.Entities.DbEntities.Base
{
    public class MasterTable : Entity
    {
        [Required]
        public virtual string Name { get; set; }
    }
}