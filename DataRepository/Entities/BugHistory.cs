﻿
using Shared.Entities.DbEntities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace  Shared.Entities.DbEntities.ErrorLoging
{
    [Table("Error_BugHistory")]
    public class BugHistory : Entity
    {
        public virtual int BigId { get; set; }
        public virtual string BugCustomId { get; set; }
        public virtual string ReplicationSteps { get; set; }
        public virtual string TestSteps { get; set; } 
        public virtual  Team ResolvedBy { get; set; }
        public virtual string FixedVersion { get; set; }
        public virtual decimal TimeInHours { get; set; }
        public virtual Priority Priority { get; set; }
        public virtual BugState State { get; set; }
        public virtual  Team FiledBy { get; set; }
        public virtual DateTime FiledDate { get; set; }
        public virtual DateTime ResolvedDate { get; set; }
        public virtual DateTime HistoryDate { get; set; }
        public virtual  Team ActionBy { get; set; }
    }
}
