﻿

using Shared.Entities.DbEntities.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace  Shared.Entities.DbEntities.ErrorLoging
{
    [Table("Error_Screenshots")]
    public class Screenshot : Entity
    {
        public virtual string Path { get; set; }
        public virtual string Name { get; set; }
        public virtual Bug Bug { get; set; }
        public virtual  Team UploadedBy { get; set; }
        public virtual DateTime UploadedDate {get;set;}
    }
}
