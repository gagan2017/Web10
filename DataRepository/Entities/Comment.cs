﻿
using Shared.Entities.DbEntities.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace  Shared.Entities.DbEntities.ErrorLoging
{
    [Table("Error_Comments")]
    public class Comment : Entity
    { 
        public virtual Bug Bug { get; set; }
        public virtual string CommentText { get; set; }
        public virtual  Team Author { get; set; }
        public virtual DateTime CommentDate { get; set; }

    }
}
