﻿using System;

namespace  Shared.Entities.DbEntities.Base
{
 
    public class Entity : IEntity
    {
        public virtual int Id { get; set; }
    }
}