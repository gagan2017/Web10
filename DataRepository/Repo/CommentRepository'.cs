﻿
using Repository.Infrastructure;
using Repository.Infrastructure.Contracts;
using Shared.Entities.DbEntities.ErrorLoging;

namespace  Repository.Repositories.ErrorLoging
{


    public interface ICommentRepository : IDataRepository<Comment>
    {

    }
    public class CommentRepository : DataRepository<Comment>, ICommentRepository
    {
        public CommentRepository(DbInfrastructureContext context) : base(context)
        {
        }
    }

}
