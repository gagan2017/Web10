﻿using Repository.Repositories.ErrorLoging;
using System;

namespace  Repository.Infrastructure.Contracts
{
    public interface IInfrastructureContext : IDisposable
    {
        IAppErrorRepository ApplicationErrors { get; }
        IBugRepository Bugs { get; }
        IBugHistoryRepository BugHistories { get; }
        IScreenshotRepository Screenshots { get; }
        IResourceRepository  Team { get; }
        ICommentRepository Comments { get; }


        int CompleteOperation();
    }
}
