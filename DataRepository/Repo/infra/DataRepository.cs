﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using  Repository.Infrastructure.Contracts;
using Shared.Entities.DbEntities.Base;

namespace  Repository.Infrastructure
{
    public class DataRepository<TEntity> : IDataRepository<TEntity>
        where TEntity : class
    {
        protected readonly DbContext Context;
        private static readonly object transaction = new object();
        internal DataRepository(DbContext context)
        {
            Context = context;
        }
        public TEntity Add(TEntity entity)
        {
            lock (transaction)
            {

                return  Context.Set<TEntity>().Add(entity);
            }
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            lock (transaction)
            {
                Context.Set<TEntity>().AddRange(entities);
            }
        }

        public IQueryable<TEntity> GetAll()
        {
            lock (transaction)
            {
                return Context.Set<TEntity>();
            }
        }

        public TEntity Get(object id)
        {
            lock (transaction)
            {
                return Context.Set<TEntity>().Find(id);
            }
        }

        public void Remove(TEntity entity)
        {
            
            lock (transaction)
            {
                Context.Set<TEntity>().Remove(entity);
            }
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            lock (transaction)
            {
                Context.Set<TEntity>().RemoveRange(entities);
            }
        }

    }
}
