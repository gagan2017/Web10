﻿using System.Collections.Generic;
using System.Linq;
 

namespace  Repository.Infrastructure.Contracts
{
    public interface IDataRepository<TEntity> where TEntity : class 
    {
        TEntity Get(object id);
        IQueryable<TEntity> GetAll();
        TEntity Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
        

    }
}
