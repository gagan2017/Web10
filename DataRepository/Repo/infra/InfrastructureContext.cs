﻿using System;
using  Repository.Infrastructure.Contracts;
using  Repository.Repositories.ErrorLoging;

namespace  Repository.Infrastructure
{
    public class InfrastructureContext : IInfrastructureContext
    {
        internal  DbInfrastructureContext _context;

        private IAppErrorRepository _appErrors;
        private IBugRepository _bugs;
        private IBugHistoryRepository _bugHistory;
        private ICommentRepository _comments;
        private IResourceRepository _Resources;
        private IScreenshotRepository _screenshots;
        

        public IAppErrorRepository ApplicationErrors => _appErrors ?? new AppErrorRepository(_context);
        public IBugRepository Bugs => _bugs ?? new BugRepository(_context);
        public IBugHistoryRepository BugHistories => _bugHistory ?? new BugHistoryRepository(_context);
        public ICommentRepository Comments => _comments ?? new CommentRepository(_context);
        public IResourceRepository  Team => _Resources ?? new  ResourceRepository(_context);
        public IScreenshotRepository Screenshots => _screenshots ?? new ScreenshotRepository(_context);

        public int CompleteOperation()
        {
            try
            {
                var count = _context.SaveChanges();
                return count;
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                Exception raise = dbEx;
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        string message = string.Format("{0}:{1}",
                            validationErrors.Entry.Entity.ToString(),
                            validationError.ErrorMessage);

                        raise = new InvalidOperationException(message, raise);
                    }
                }
                throw raise;
            }
            finally
            {
                Dispose();
            }


        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}