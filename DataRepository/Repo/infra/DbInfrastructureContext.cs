﻿using Shared.Entities.DbEntities.ErrorLoging;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace  Repository.Infrastructure
{
    public class  DbInfrastructureContext : DbContext
    {
        public DbInfrastructureContext() : base("DefaultConnection") { }
        
        protected override void OnModelCreating(DbModelBuilder pModelBuilder)
        {
            base.OnModelCreating(pModelBuilder);
            pModelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            pModelBuilder.Types().Configure(pEntity => pEntity.ToTable("tarams" + pEntity.ClrType.Name));
        }

         
            

        public DbSet<ApplicationError> AppErrors { get; set; }
        public DbSet<Bug> Bugs { get; set; }
        public DbSet<BugHistory> BugHistories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet< Team>  Teams { get; set; }
        public DbSet<Screenshot> Screenshots { get; set; }

        
    }
       
}
 
