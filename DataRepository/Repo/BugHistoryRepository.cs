﻿
using Repository.Infrastructure;
using Repository.Infrastructure.Contracts;
using Shared.Entities.DbEntities.ErrorLoging;

namespace  Repository.Repositories.ErrorLoging
{ 

    public interface IBugHistoryRepository : IDataRepository<BugHistory>
    {

    }
    public class BugHistoryRepository : DataRepository<BugHistory>, IBugHistoryRepository
    {
        public BugHistoryRepository(DbInfrastructureContext context) : base(context)
        {
        }
    }

}
