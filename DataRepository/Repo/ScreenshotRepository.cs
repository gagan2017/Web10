﻿using System;
using Repository.Infrastructure;
using Repository.Infrastructure.Contracts;
using Shared.Entities.DbEntities.ErrorLoging;

namespace  Repository.Repositories.ErrorLoging
{
    public interface IScreenshotRepository : IDataRepository<Screenshot>
    {
         
    }
    public class ScreenshotRepository : DataRepository<Screenshot>, IScreenshotRepository
    {
        public ScreenshotRepository( DbInfrastructureContext context) : base(context)
        {
        }

      
    }

}


