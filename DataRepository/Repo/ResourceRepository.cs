﻿using Shared.Entities.DbEntities.ErrorLoging;
using  Repository.Infrastructure;
using  Repository.Infrastructure.Contracts;

namespace  Repository.Repositories.ErrorLoging
{


    public interface IResourceRepository : IDataRepository< Team>
    {

    }
    public class  ResourceRepository : DataRepository< Team>, IResourceRepository
    {
        public  ResourceRepository(DbInfrastructureContext context) : base(context)
        {
        }
    }

}
