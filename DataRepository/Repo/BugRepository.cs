﻿
using Repository.Infrastructure;
using Repository.Infrastructure.Contracts;
using Shared.Entities.DbEntities.ErrorLoging;

namespace  Repository.Repositories.ErrorLoging
{


    public interface IBugRepository : IDataRepository<Bug>
    {

    }
    public class BugRepository : DataRepository<Bug>, IBugRepository
    {
        public BugRepository(DbInfrastructureContext context) : base(context)
        {
        }
    }

}
