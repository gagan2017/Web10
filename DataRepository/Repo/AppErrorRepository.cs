﻿using Repository.Infrastructure;
using Repository.Infrastructure.Contracts;
using Shared.Entities.DbEntities.ErrorLoging;

namespace  Repository.Repositories.ErrorLoging
{


    public interface IAppErrorRepository : IDataRepository<ApplicationError>
    {

    }
    public class AppErrorRepository : DataRepository<ApplicationError>, IAppErrorRepository
    {
        public AppErrorRepository(DbInfrastructureContext context) : base(context)
        {
        }
    }
   
}
