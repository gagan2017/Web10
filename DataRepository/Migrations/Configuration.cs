namespace DataRepository.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Repository.Infrastructure.DbInfrastructureContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Repository.Infrastructure.DbInfrastructureContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            context.Teams.AddOrUpdate(
              p => p.Name,
              new Shared.Entities.DbEntities.ErrorLoging. Team { Name = "Sreemanoj",
                  Role = Shared.Entities.DbEntities.ErrorLoging. Role.Dev }
               
            );

        }
    }
}
